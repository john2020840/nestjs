# Nest
Nest CI/CD 파이프라인 예제
#### 환경
- Jest version is 28.1.3.
- TypeScript version is 4.7.4.
- NestJS framework version is 9.0.0.
- ESLint version is 8.0.1.
- Prettier version is 2.3.2.
- Supertest version is 6.1.3.
- ts-jest version is 28.0.8.
#### 파이프라인 정보
- 